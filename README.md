# Zine

This is template for making one page zines. 
Code is taken from [zine-machine](https://zine-machine.glitch.me/) and unnecessary parts were removed.

# Usage
Fill up content you want. Make you own zine! Then start web browser and open zine.html in it choose print page.
Be sure to remove additional stuff that browser puts in, like  date time url etc.
From there print it right away or save it to a pdf for later printing.

# Share zines
If you make a zine using this template put it in `/zines` folder and issue a pull request.

# Contribute
Check [issues](), add new feature or fix old bugs. 

# License
**GPLv3+**: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>

This is *free* software: you are free to change and redistribute it.

There is **NO WARRANTY**, to the extent permitted by law.

